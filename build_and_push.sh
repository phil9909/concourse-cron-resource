#!/usr/bin/env bash

set -eu -o pipefail

docker build -t phil9909/concourse-cron-resource .
docker push phil9909/concourse-cron-resource