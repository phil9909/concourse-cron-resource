package parser_test

import (
	"testing"
	"time"

	"gitlab.com/phil9909/concourse-cron-resource/pkg/parser"
)

func parseTime(t string) time.Time {
	parsed, err := time.Parse("2006-01-02 15:04:05 MST", t)
	if err != nil {
		panic(err)
	}
	return parsed
}

func positiveTest(t *testing.T, cron string, input string, output string) {
	res, err := parser.FindLastResource(cron, parseTime(input))
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if res == nil {
		t.Errorf("result is nil")
		t.FailNow()
	}
	expected := parseTime(output)
	if !res.Equal(expected) {
		t.Errorf("time did not match\nexpected: %v\ngot     : %v\nfor input %s %s", expected, res, input, cron)
	}
	if res.Location().String() != expected.Location().String() {
		t.Errorf("did not preserve location. expected: %v got: %v", expected.Location(), res.Location())
	}
}

func TestFindLastResourceSimple(t *testing.T) {
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 08:40:00 CEST", "2019-08-09 00:05:00 CEST")
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 08:40:00 PST", "2019-08-09 00:05:00 PST")
}

func TestTimeZone(t *testing.T) {
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 00:00:00 CEST", "2019-08-08 00:05:00 CEST")
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 01:00:00 CEST", "2019-08-09 00:05:00 CEST")
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 02:00:00 CEST", "2019-08-09 00:05:00 CEST")
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 03:00:00 CEST", "2019-08-09 00:05:00 CEST")
}

func TestFindLastResourceExactly(t *testing.T) {
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 00:05:00 CEST", "2019-08-08 00:05:00 CEST")
	positiveTest(t, "0 5 0 * 8 *", "2019-08-09 00:05:00 PST", "2019-08-08 00:05:00 PST")
}

func TestFindLastResourceError(t *testing.T) {
	res, err := parser.FindLastResource("invalid", parseTime("2019-08-09 08:40:00 CEST"))
	if err == nil {
		t.Errorf("expected error")
	}
	if res != nil {
		t.Errorf("result is not nil")
	}
}
