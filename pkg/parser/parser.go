package parser

import (
	"fmt"
	"time"
	"unsafe"
)

// #cgo CFLAGS: -g -Wall
// #include <stdlib.h>
// #include "../../ccronexpr/ccronexpr.c"
import "C"

// FindLastResource takes a string in cron `cron` syntax and a timestamp `t` and returns a timestamp, which is:
// - before t
// - a valid time for the cron
// - as close to t as possible
func FindLastResource(cron string, t time.Time) (*time.Time, error) {
	ccron := C.CString(cron)
	defer C.free(unsafe.Pointer(ccron))

	ptr := C.struct___0{}
	_, offset := t.Zone()

	var cerr *C.char
	C.cron_parse_expr(ccron, &ptr, &cerr)
	if cerr != nil {
		return nil, fmt.Errorf("Error in c code: %v", C.GoString(cerr))
	}

	cprev := C.cron_prev(&ptr, C.long(t.Unix() + int64(offset)))
	prev := time.Unix(int64(cprev), 0)
	
	prev = prev.Add(time.Duration(offset) * time.Second * -1)
	prev = prev.In(t.Location())

	return &prev, nil
}
