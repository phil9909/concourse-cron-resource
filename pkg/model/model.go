package model

import timeresource "github.com/concourse/time-resource/models"

type InRequest struct {
	Source  Source  `json:"source"`
	Version Version `json:"version"`
}

type InResponse struct {
	Version Version `json:"version"`
}

type CheckRequest struct {
	Source  Source  `json:"source"`
	Version Version `json:"version"`
}

type CheckResponse []Version

type Source struct {
	Cron     string                 `json:"cron"`
	Location *timeresource.Location `json:"location"`
}

type Version struct {
	Time string `json:"time"`
}
