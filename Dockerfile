FROM golang:1.12 as builder
WORKDIR /opt/concourse-cron-resource/
COPY . ./
RUN mkdir out
RUN go build -o out/check "gitlab.com/phil9909/concourse-cron-resource/cmd/check"
RUN go build -o out/in "gitlab.com/phil9909/concourse-cron-resource/cmd/in"

FROM gcr.io/distroless/cc
WORKDIR /opt/resource
COPY --from=builder  /opt/concourse-cron-resource/out/ ./
ENTRYPOINT ["/opt/resource/check"]