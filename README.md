# Usage

The following example provides a new version dialy at 4 AM (in German-time CET or CEST)
```yaml
resource_types:
- name: cron
  type: docker-image
  source:
    repository: phil9909/concourse-cron-resource
    tag: latest

resources:
- name: 4am
  type: cron
  source:
    cron: "0 0 4 * * *"
    location: "Europe/Berlin"
```

**This resource is always availabe.** So if you use the example above and do a `fly set-pipeline` at e.g. 10 PM the pipeline will be trigger immediately and then again at 4 AM the next morning.



# About

This Project is licensed under the Apache 2.0 Licence.

It uses:
* The [concourse/time-resource](https://github.com/concourse/time-resource) licensed under Apache 2.0
* The [staticlibs/ccronexpr](https://github.com/staticlibs/ccronexpr) licensed under Apache 2.0
* The [Go programming language](https://golang.org/) ([LICENSE](https://golang.org/LICENSE))