package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"gitlab.com/phil9909/concourse-cron-resource/pkg/model"
	"gitlab.com/phil9909/concourse-cron-resource/pkg/parser"
)

func main() {
	var request model.CheckRequest

	err := json.NewDecoder(os.Stdin).Decode(&request)
	if err != nil {
		fmt.Fprintln(os.Stderr, "parse error:", err.Error())
		os.Exit(1)
	}

	now := time.Now()
	if request.Source.Location != nil {
		now = now.In((*time.Location)(request.Source.Location))
	}

	res, err := parser.FindLastResource(request.Source.Cron, now)

	versions := []model.Version{
		model.Version{
			Time: res.String(),
		},
	}
	json.NewEncoder(os.Stdout).Encode(versions)
}
